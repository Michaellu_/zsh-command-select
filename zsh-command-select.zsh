#!/usr/bin/env zsh
0="${${ZERO:-${0:#$ZSH_ARGZERO}}:-${(%):-%N}}"
0="${${(M)0:#/*}:-$PWD/$0}"

typeset -g _func_base="${0:h}"
typeset -g _file_base="$HOME/.config/cli_select"

touch "${_file_base}/desc.txt"
touch "${_file_base}/cmds.sh"

if [[ $PMSPEC != *f* ]] {
  fpath+=( "${_func_base}/functions" )
}

autoload -Uz select_cmd
autoload -Uz new_cmd

# Read desc.txt file line by line & save it
function _refresh_cmds() {
	_desc_list=()
	while read -r line
	do
		# Split line by , into array
		_array=("${(@s/,/)line}")
		
		_name="${_array[1]}" # Index starts at 1
		_desc="${_array[2]}"
		_alias="${_array[3]}"
	
		# : is the seperator between item and description for zsh
		_desc_list+="${_name}:${_alias}, ${_desc}"
	done < "${_file_base}/desc.txt"

	# Get the new aliases/functions
	source "${_file_base}/cmds.sh"
}

_refresh_cmds

function _template() {
  _arguments \
    '1: :->subcmds' && return 0

  if (( $#subcmds == 0 )); then
    _describe -t commands 'Descriptions' _desc_list
    return
  fi
}

compdef _template select_cmd
